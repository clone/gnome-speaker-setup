# This file is part of gnome-speaker-setup.
#
# Copyright 2009 Lennart Poettering
#
# gnome-speaker-setup is free software; you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as
# published by the Free Software Foundation; either version 2.1 of the
# License, or (at your option) any later version.
#
# gnome-speaker-setup is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with gnome-speaker-setup; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
# USA.

gnome-speaker-setup: gnome-speaker-setup.vala
	valac -g --save-temps \
		--pkg posix \
		--pkg gee-1.0 \
		--pkg gtk+-2.0 \
		--pkg libcanberra \
		--pkg libpulse --vapidir=. --Xcc=-lpulse --Xcc=-lpulse-mainloop-glib \
		$^

clean:
	rm -f gnome-speaker-setup gnome-speaker-setup.c

release:
	test "x$$VERSION" != x
	git tag v$$VERSION -m "gnome-speaker-setup $$VERSION"
	git archive --prefix=gnome-speaker-setup-$$VERSION/ v$$VERSION | gzip -c > gnome-speaker-setup-$$VERSION.tar.gz
	scp gnome-speaker-setup-$$VERSION.tar.gz tango:public/
